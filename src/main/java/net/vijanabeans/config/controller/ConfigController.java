package net.vijanabeans.config.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.vijanabeans.config.middleware.QueueProducer;

@RestController
public class ConfigController {
    @Autowired
    private QueueProducer producer;    

    public ConfigController(QueueProducer producer) {
        this.producer = producer;
    }

    @GetMapping(value = "/sender")
    public ResponseEntity<String> producer(@RequestParam("message")String message){
        producer.sendMessage(message);
        return ResponseEntity.ok("Message sent successfully");
    }
}
