package net.vijanabeans.config.middleware;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class QueueConsumer {
   
    private static final Logger LOGGER = LoggerFactory.getLogger(QueueConsumer.class);
    
    @RabbitListener(queues = {"${karest.queue.name}"})
    public void consume(String message){
        LOGGER.info(String.format("Recevied messo -> %s", message));
    }

}
