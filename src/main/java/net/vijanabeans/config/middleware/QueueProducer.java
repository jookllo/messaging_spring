
package net.vijanabeans.config.middleware;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class QueueProducer {

    @Value("${karest.exchange.name}")
    private String exchange;
    
    @Value("${karest.routing.key}")
    private String routingkey;

    private static final Logger LOGGER = LoggerFactory.getLogger(QueueProducer.class);

    private RabbitTemplate rabbitTemplate;

    public QueueProducer(RabbitTemplate rabbitTemplate){
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessage(String message){
        LOGGER.info(String.format("Send -> %s",message));
        rabbitTemplate.convertAndSend(exchange,routingkey,message);
    }
}
