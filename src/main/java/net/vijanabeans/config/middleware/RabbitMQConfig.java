package net.vijanabeans.config.middleware;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Value("${karest.queue.name}")
    private String queueName;

    @Value("${karest.exchange.name}")
    private String exchange;
    
    @Value("${karest.routing.key}")
    private String routingkey;


    @Bean
    public Queue queue(){
        return new Queue(queueName,false);
    }

    @Bean
    public DirectExchange exchange(){
        return new DirectExchange(exchange);
    }

    @Bean
    public Binding binding(Queue queue, DirectExchange exchange){
        return BindingBuilder.bind(queue())
                .to(exchange)
                .with(routingkey);
    }

    
}
